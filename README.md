# Bobbapp: configs

This project is being used as a GitOps repository (managed by FluxCD and Helm-Operator running in Kubernetes) that watches changes in Helm templates (`bobbapp` directory).


More info: https://gitlab.com/azizos/bobbapp/env/infra